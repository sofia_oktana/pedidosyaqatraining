import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page'
import confirmUbicationModal from '../pageobjects/confirmUbicationModal.page';
import searchResult from '../pageobjects/searchResults.page';
import restaurantResult from '../pageobjects/restaurantResult';

defineSupportCode(function({ Given, When, Then }) {

    Given (/^I am on a restaurant list in address and department$/, function(table){
        const hashes = table.hashes()
        let address = hashes[0]['address']
        let department = hashes[0]['department']

        homePage.open()
        browser.maximizeWindow
        browser.pause(9000)
        homePage.clickCityAddress()
        homePage.setCity(department)
        homePage.setAddress(address)
        homePage.clickBuscarButton()
        confirmUbicationModal.clickConfirmButton()
        browser.pause(9000)
    })

    When(/^I select "([^"]*)" restaurant$/, function(restaurant) {
        searchResult.sendRestaurant(restaurant)
    })

    When (/^I add "([^"]*)" to the order$/, function(food) {
        restaurantResult.foodSearch(food)
        browser.pause(9000)
    })

    When (/^I change the amount of "([^"]*)" to "([^"]*)"$/, function(food, amount) {



    })








})
