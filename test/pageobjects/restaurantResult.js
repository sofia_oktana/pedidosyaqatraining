class restaurantResult {
    get foodSearchField()       {return browser.element('#menuSearch')}
    get foodSearchResult()      {return browser.elements('#menu > div > section:nth-child(13) > ul > li > div > h4 > mark')}
    get allFoods()              {return browser.elements('.productName')}
    get foodAmountDropdown()    {return browser.element('div.productQuantity.dropdown')}
    get foodAmounts()           {return browser.elements('div.productQuantity.dropdown > select > option')}



    foodSearch(food) {
        this.allFoods.waitForVisible(9000)

        let res = this.allFoods.value.length
        console.log('Cantidad de elementos: ' + res)
        for(let i = 0; i <= res; i++){
            let resFoodName = this.allFoods.value[i].getText();
            console.log('ResfoodName = ' + resFoodName)
            if(resFoodName === food){
                this.allFoods.value[i].click();
                break;
            }

        }
    }

    setFoodAmounts(amount) {
        this.foodAmountDropdown.waitForVisible(9000)
        this.foodAmountDropdown.value.click()

        let resAmount = this.foodAmounts.value.length
        console.log('Cantidad de elementos: ' + resAmount)

        for(let i = 0; i <= resAmount; i++){
            let amountValue = this.foodAmounts.value[i].getText();
            console.log('AmountValue = ' + amountValue)
            if(amountValue === amount){
                this.amountValue.value[i].click()
                break;
            }
        }

    }






    // setFoodSearch(food) {
    //     this.foodSearchField.waitForVisible(9000)
    //     this.foodSearchField.setValue(food)
    //     this.foodSearchResult.waitForVisible(9000)
    //     let res = this.foodSearchResult.value.length
    //     console.log('Cantidad de elementos: ' + res);
    //     for(let i = 0; i <= res ; i++){
    //         let resFoodName = this.foodSearchResult.value[i].getText();
    //         console.log('ResFoodName = ' + resFoodName);
    //         if( resFoodName === food){
    //             this.foodSearchResult.value[i].click();
    //             break;
    //         }
    //     }
    // }



}

export default new restaurantResult()