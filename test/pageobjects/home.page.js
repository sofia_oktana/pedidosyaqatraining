import Page from './page'

class HomePage extends Page {

  get logo ()           { return browser.element('.logo') }
  get ingresarLink ()   { return browser.element('#lnkLogin') }
  get userNameLink ()   { return browser.element('#lnkUserName > div.left.top-link') }
  get ciudadDropdown () { return browser.element('#selectCity_chosen') }
  get addressField ()   { return browser.element('#address') }
  get buscarButton ()   { return browser.element('#search') }
  get cities ()         { return browser.elements('.active-result')}

  open () {
    super.open('/')
    this.logo.waitForVisible()
  }

  clickIngresarLink () {
    this.ingresarLink.waitForVisible()
    this.ingresarLink.click()
  }

  clickBuscarButton () {
    this.buscarButton.waitForVisible()
    this.buscarButton.click()
  }

  getUserNameLink () {
    this.userNameLink.waitForVisible()
    return this.userNameLink.getText()
  }

  clickCityAddress (){
    this.ciudadDropdown.waitForVisible(9000)
    this.ciudadDropdown.click()
  }
  
  setCity (department){
    this.cities.waitForVisible(9000)
    let elementCity
    for(let i = 0; i < this.cities.value.length; i++){
      elementCity = this.cities.value[i].getText()
      if(elementCity === department){
        this.cities.value[i].click()
        break
      }
    }

  }

  setAddress (address) {
  this.addressField.waitForVisible(9000)
  this.addressField.setValue(address)
  }


}

export default new HomePage()
