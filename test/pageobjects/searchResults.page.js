class searchResult {

    get titleSearch()  {return browser.element('.addressTitle > h1')}
    get restaurantsList () {return browser.elements('.restaurant-wrapper')}
    get searchRestaurantField () {return browser.element('#searchList')}
    get searchIcon () {return browser.element('.secondary.button')}
    get searchResultList () {return browser.elements('.arrivalName')}


    getTitleEspected(){
        this.titleSearch.waitForVisible(9000)

        return this.titleSearch.getText()
    }

    isARestaurantPresent(){
        let list = this.restaurantsList.value.length;
        list = list - 2;
        console.log('Cantidad de elementos: ' + list);
        return list;
        //return list > 0 ?  true : false;
    }

    sendRestaurant(restaurant) {
        this.searchRestaurantField.waitForVisible(9000)
        this.searchRestaurantField.setValue(restaurant)
        this.searchIcon.click()

        let rest = this.searchResultList.value.length;

        console.log('Cantidad de elementos: ' + rest);

        for(let i = 0; i <= rest ; i++){
            let restName = this.searchResultList.value[i].getText();
            console.log('RestName = ' + restName);
            if( restName === restaurant){
                this.searchResultList.value[i].click();
                break;
            }
        }


        
    }


}
export default new searchResult()